module.exports = {
  apps: [
    {
      name: "smarketpod",
      cwd: "boilerplate",
      script:
        "./node_modules/.bin/moleculer-runner services/*.service.js apps/*.app.js",
      args: "services/*.service.js",
      error_file: "./logs/err.log",
      out_file: "./logs/out.log",
    },
  ],
};
