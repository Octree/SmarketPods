FROM node:18-alpine

RUN apk add --no-cache git python3 alpine-sdk 
RUN yarn global add pm2

WORKDIR /app
COPY . /app
RUN yarn

EXPOSE 3000
VOLUME /app/boilerplate/jwt /app/boilerplate/actors

CMD [ "pm2-runtime", "ecosystem.config.js" ]