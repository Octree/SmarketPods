const { MIME_TYPES } = require("@semapps/mime-types");
const { ACTIVITY_TYPES } = require("@semapps/activitypub");

const OFFER_TYPE = "schema:Offer";

module.exports = {
  name: "smarket",
  events: {
    "activitypub.inbox.received": {
      async handler(ctx) {
        const { activity, recipients } = ctx.params;
        const objectUri =
          activity.object?.id || activity.object?.["@id"] || activity.object;

        if (activity?.type === "Accept") {
          for (let recipientUri of recipients) {
            const offer = await ctx.call("activitypub.object.get", {
              objectUri,
              actorUri: recipientUri,
            });

            const offerType = offer.type || offer["@type"];
            if (offerType === OFFER_TYPE) {
              // If offer is no longer valid, send Reject to acceptor
              if (
                offer["schema:validThrough"] &&
                new Date(offer["schema:validThrough"]) < new Date()
              ) {
                try {
                  const creator = await ctx.call("activitypub.actor.get", {
                    actorUri: offer["dc:creator"],
                  });
                  await ctx.call("activitypub.outbox.post", {
                    collectionUri: creator.outbox,
                    type: ACTIVITY_TYPES.REJECT,
                    actor: creator.id,
                    object: offer.id,
                    target: activity.actor,
                    to: activity.actor,
                  });
                  this.logger.info(
                    `Offer's rejection sent to ${activity.actor}`
                  );
                } catch (e) {
                  console.error(e);
                  this.logger.warn(
                    `Can't send offer's rejection to ${activity.actor}`
                  );
                }
              }
              // If offer is valid
              else {
                // Update targeted Offer
                try {
                  const validThrough = `${new Date().toISOString()}`;
                  await ctx.call("ldp.resource.put", {
                    resource: {
                      ...offer,
                      "schema:buyer": activity.actor,
                      "schema:validThrough": validThrough,
                    },
                    contentType: MIME_TYPES.JSON,
                    webId: "system",
                  });
                  this.logger.info(`Offer ${objectUri} updated`);
                } catch (e) {
                  console.error(e);
                  this.logger.warn(`Unable to update object ${objectUri}`);
                }

                // Publish offer's update to followers
                try {
                  const creator = await ctx.call("activitypub.actor.get", {
                    actorUri: offer["dc:creator"],
                  });
                  await ctx.call("activitypub.outbox.post", {
                    collectionUri: creator.outbox,
                    type: ACTIVITY_TYPES.UPDATE,
                    actor: creator.id,
                    object: offer.id,
                    target: creator.followers,
                    to: creator.followers,
                  });
                  this.logger.info(
                    `Offer's update published to followers for ${objectUri}`
                  );
                } catch (e) {
                  console.error(e);
                  this.logger.warn(
                    `Unable to publish offer's update to followers for ${objectUri}`
                  );
                }
              }
            }
          }
        }
      },
    },
  },
};
