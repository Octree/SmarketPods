const { WEBHOOK_URL } = require("../config/config");

module.exports = {
  name: "webhook",
  events: {
    "activitypub.inbox.received": {
      async handler(ctx) {
        if (WEBHOOK_URL)
          try {
            await fetch(WEBHOOK_URL, {
              method: "POST",
              body: JSON.stringify(ctx.params),
              headers: {
                "Content-Type": "application/json",
              },
            });
            this.logger.info(
              `Webhook sent to ${WEBHOOK_URL} for new inbox message.`
            );
          } catch (error) {
            console.error(error);
            this.logger.warn(`Can't send webhook to ${WEBHOOK_URL}`);
          }
      },
    },
  },
};
